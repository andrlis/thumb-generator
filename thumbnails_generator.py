import os
import cv2
import argparse

VIDEO_FORMATS = ['.WEBM', '.MPG', '.MP2', '.MPEG', '.MPE', '.MPV', '.OGG', '.MP4', '.M4P', '.M4V', '.AVI', '.WMV',
                 '.MOV', '.QT', '.FLV', '.SWF', '.AVCHD']


def video_to_frames(video_filename, frequency):
    """Extract frames from video"""
    f = os.path.isfile(video_filename)
    cap = cv2.VideoCapture(video_filename)
    video_length = int(cap.get(cv2.CAP_PROP_FRAME_COUNT)) - 1
    frames = []
    if cap.isOpened() and video_length > 0:
        frame_ids = []
        if video_length >= frequency:
            frame_ids = list(range(0, video_length, frequency))
            frame_ids.append(video_length - 1)
        count = 0
        success, image = cap.read()
        while success:
            if count in frame_ids:
                frames.append(image)
            success, image = cap.read()
            count += 1
    cap.release()
    return frames


def image_to_thumb(img, size):
    """Create thumbs from image"""
    height, width, channels = img.shape
    if size == 0:
        thumb = img
    if width >= size:
        r = (size + 0.0) / width
        max_size = (size, int(height * r))
        thumb = cv2.resize(img, max_size, interpolation=cv2.INTER_AREA)
    return thumb


def get_list_of_videos(dir_name):
    list_of_file = os.listdir(dir_name)
    all_files = list()
    for entry in list_of_file:
        full_path = os.path.join(dir_name, entry)
        if os.path.isdir(full_path):
            all_files = all_files + get_list_of_videos(full_path)
        else:
            _, file_ext = os.path.splitext(full_path)
            if file_ext.upper() in VIDEO_FORMATS:
                all_files.append(full_path)

    return all_files


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Thumbnails generator')
    parser.add_argument('-i', '--indir', type=str, help='Input directory with sources')
    parser.add_argument('-s', '--size', type=int, default=160, help='Thumbnail size. [640, 320, 160]')
    parser.add_argument('-f', '--frequency', type=int, default=5, help='Make thumb each N second')
    args = parser.parse_args()

    input_directory = args.indir

    try:
        videos = get_list_of_videos(input_directory)

        for video in videos:
            frames = video_to_frames(video, args.frequency)
            result_directory = os.path.join(os.path.dirname(video),
                                            "%s_thumbs" % (os.path.splitext(os.path.basename(video))[0]))
            if not os.path.isdir(result_directory):
                os.mkdir(result_directory)

            for i in range(len(frames)):
                thumb = image_to_thumb(frames[i], args.size)
                cv2.imwrite('%s/%d.png' % (result_directory, i), thumb)

    except Exception as e:
        print("Oops! Something went wrong.. :( :\n{}".format(e))
